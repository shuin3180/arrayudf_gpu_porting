#include<iostream>
#include<cstdlib>
#include<cstdio>
#include<cuda.h>

__global__ void hello(){
  printf("Hello from block%d, thread%d\n",blockIdx.x,threadIdx.x);
  //std::cout<<"Hello from  block "<<blockIdx.x<<", thread "<<threadIdx.x<<std::endl;
}

int main(int argc, char**argv){
  hello<<<10,10>>>();
  cudaDeviceSynchronize();
  return 0;
}
