#include<stdio.h>
#include<iostream>
#include<cuda.h>
#include<cstdlib>
#include<string>
#include<thrust/host_vector.h>
#include<thrust/device_vector.h>

using namespace std;

//https://stackoverflow.com/questions/14038589/what-is-the-canonical-way-to-check-for-errors-using-the-cuda-runtime-api
//You can then wrap each API call with the gpuErrchk macro, which will process the return status of the API call it wraps, for example:
#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true){
  if (code != cudaSuccess) 
  {
    fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
    if (abort) exit(code);
  }
}
//in kernel driver, invoke nvidia_p2p_get_pages().


//GPUDirect RDMA ref : pinning GPU mem
//Correct behavior requires using cuPointerSetAttribute() on the memory address to enable proper synchronization behavior in the CUDA driver. See section Synchronization and Memory Ordering.
//This is required so that the GPU memory buffer is treated in a special way by the CUDA driver, so that CUDA memory transfers are guaranteed to always be synchronous with respect to the host. See Userspace API for details on cuPointerSetAttribute().


/*****syslevel
void pin_buffer(void *address, size_t size){
  unsigned int flag = 1;
  CUresult status = cuPointerSetAttribute(&flag, CU_POINTER_ATTRIBUTE_SYNC_MEMOPS, address);
  if (CUDA_SUCCESS == status) {
    // GPU path
    pass_to_kernel_driver(address, size);
  } else {
    // CPU path
    // ...
  }
}
*/

class Managed{
public:
  void *operator new(size_t len){
    void *ptr;
    cudaMallocManaged(&ptr, len);
    cudaDeviceSynchronize();
    return ptr;
  }

  void operator delete(void *ptr){
    cudaDeviceSynchronize();
    cudaFree(ptr);
  }
};

class testArray : public Managed{
  int length;
  char* data;

public:
  testArray(const string &s){
    FILE* f = fopen(s.c_str(),"r");
    cudaMallocManaged(&data,14*7);
    for(int i=0;i<7;i++){
      fread(data+14*i,1,14,f);
    }
  }
  __device__ int retrieve(int x){
    return data[x];
  }
};

__global__ void performAction(testArray* arr){
  int bx=blockIdx.x;
  int tx=threadIdx.x;
  printf("bid %d, tid %d reads %c\n",bx,tx,arr->retrieve(bx*7+tx));
}
int main(){
  int devCount;
  int device;

  testArray *c= new testArray("test_io_file.csv");
  performAction<<<7,14>>>(c); 
  cudaDeviceSynchronize();






  /****
  unsigned long long datum=1000;
  unsigned long long** ptr;
  cudaDeviceProp devProp;
  cudaGetDeviceCount(&devCount);
  printf("%d\n",devCount);
  **
  for(int i=0;i<devCount;i++){
    cudaGetDeviceProperties(&devProp,0);
    if(!devProp.canMapHostMemory){
      return 0;
    }
  }
  **

  //cudaGetDevice(&device);
  cudaError_t err=cudaSetDeviceFlags(cudaDeviceMapHost);
  if(cudaSuccess==err)
    printf("flag set\n");
  else if(cudaErrorInvalidDevice==err)
    printf("err invalid dev\n");
  else if(cudaErrorSetOnActiveProcess==err)
    printf("err set on active proscess\n");
  //cudaMalloc((void**)ptr,sizeof(unsigned long long));
  // cuPointerGetAttribute(&datum, CU_POINTER_ATTRIBUTE_SYNC_MEMOPS,ptr);


  //MemAlloc area
  cudaMallocManaged(
  //MemAlloc area end

  //kernel call area
  //kernel call area end
  cudaDeviceSynchronize();
*/
  return 0;
}
