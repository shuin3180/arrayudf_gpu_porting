/* Original code from Bin Dong */

#ifndef __CUDA__arrayudf__stencil__
#define __CUDA__arrayudf__stencil__


#include <cuda_runtime.h>
#include <cuda.h>

using namespace std;


template <class T>
class Stencil{
private:
  T value;
  unsigned long long* my_location;
  unsigned long long my_g_location_rm;

  unsigned long long chunk_id;
  T *chunk_data_pointer;
  unsigned long long chunk_data_size;
  unsigned long long *chunk_dim_size;

  int dims;
  int x
