#include <cuda.h>
#include <thrust/device_malloc_allocator.h>
#include <thrust/device_vector.h>
#include <thrust/system_error.h>
#include <thrust/system/cuda/error.h>
#include <iostream>

class Managed{
public:
  void *operator new(size_t len){
    void *ptr;
    cudaMallocManaged(&ptr, len);
    cudaDeviceSynchronize();
    return ptr;
  }

  void operator delete(void *ptr){
    cudaDeviceSynchronize();
    cudaFree(ptr);
  }
};

///https://stackoverflow.com/questions/9007343/mix-custom-memory-management-and-thrust-in-cuda
//https://devtalk.nvidia.com/default/topic/987577/-thrust-is-there-a-managed_vector-with-unified-memory-do-we-still-only-have-device_vector-cuda-thrust-managed-vectors-/
template <class T>
class managed_allocator<T> : public thrust::device_malloc_allocator<T>{

  //shorthand for name of base class
  typedef thrust::device_malloc_allocator<T> super_t;

  //ref of base class's typedefs
  typedef typename super_t::pointer pointer;
  typedef typename super_t::size_type size_type;
  pointer allocate(size_type n){
    std::cout << "self_allocator() allocating size of "<< n <<std::endl;
    T* ret = nullptr;

    cudaError_t err = cudaMallocManaged(&ret,n*sizeof(T),cudaMemAttachGlobal);
    if(err!=cudaSuccess){
      throw thurst::system_error(err,thrust::cuda_category(),"self_allocator::allocate() : cudaMallocManaged");
    }
    return thrust::device_pointer_cast(ret);
    //return super_t::allocate(n);
  }

  void deallocate(pointer ptr,size_type){
    cudaError_t err = cudaFree(thrust::raw_pointer_cast(ptr));
    if(err!=cudaSuccess){
      throw thurst::system_error(err,thrust::cuda_category(),"self_allocator::deallocate() : cudaFree");
    }

    return;
    
};
