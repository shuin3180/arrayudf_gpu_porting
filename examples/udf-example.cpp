/*
 *ArrayUDF Copyright (c) 2017, The Regents of the University of California, through Lawrence Berkeley National Laboratory (subject to receipt of any required approvals from the U.S. Dept. of Energy).  All rights reserved.

 *If you have questions about your rights to use or distribute this software, please contact Berkeley Lab's Innovation & Partnerships Office at  IPO@lbl.gov.

* NOTICE. This Software was developed under funding from the U.S. Department of Energy and the U.S. Government consequently retains certain rights. As such, the U.S. Government has been granted for itself and others acting on its behalf a paid-up, nonexclusive, irrevocable, worldwide license in the Software to reproduce, distribute copies to the public, prepare derivative works, and perform publicly and display publicly, and to permit other to do so. 
 */
/**
 *
 * Email questions to {dbin, kwu, sbyna}@lbl.gov
 * Scientific Data Management Research Group
 * Lawrence Berkeley National Laboratory
 *
 */
/*
 * HexagonalED fixing code from udf_example to possion_2d from paper
 *
 * for experience for developing arrayUDF for ML workload
 *
 * 2019.06.28
 *
 *
 */


#include <iostream>
#include <stdarg.h>
#include <vector>
#include <stdlib.h>
#include "array_udf_array.h"
#include "mpi.h"

using namespace std;

inline float possion_2D(const Stencil<float> &c){
  return 4.0*c(0,0)-c(-1,0)-c(0,1)-c(1,0)-c(-1,0);
}

int main(int argc, char *argv[])
{
  //Init the MPICH
  MPI_Init(&argc, &argv);
  
  // set up the chunk size and the overlap size
  std::vector<int> chunk_size; chunk_size.push_back(10000); chunk_size.push_back(10000);
  std::vector<int> overlap_size; overlap_size.push_back(1); overlap_size.push_back(1);

  //Orginal data set
  Array<float> *S1 = new Array<float>("test_S1.h5p",       "/arrayUDF_retest", "/arrayUDF_retest/S1", chunk_size, overlap_size);

  //Results data sets
  Array<float> *S1_ret = new Array<float>("test_S1-ret.h5p",       "/arrayUDF_retest", "/arrayUDF_retest/S1");

  //Apply myfunc1, myfunc2, myfunc3 to A, seperately.
  //Results are stored onto B, C , D, accordingly
  S1->Apply(possion_2D,S1_ret);

  //Clear
  delete S1;
  delete S1_ret;
  
  MPI_Finalize();
  
  return 0; 
}
